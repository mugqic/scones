#!/bin/sh

#Usage filterAnnotCNV.sh ftout_input($1) TelomerCentro1MB($2) genePos($3) DGVmap($4) microSat($5) repeat_mask($6) chrLen_file($7) output($8) prefix($9) 

module load mugqic/bedtools/2.25.0
##params
execPath=$(dirname $0)
INPUT_FILE=$1
EXCL_MAP=$2
GEN_POS=$3
DGV_MAP=$4
MICROSAT_POS=$5
REPEAT_MASK_POS=$6
CHR_LEN=$7
OUTPUT_FILE=$8
UTR_POS="$9.tmpUTRpos.txt"
if [ $# -eq 10 ] 
then
	CNV_PROX=${10}
else
	CNV_PROX="0"
fi
#cleaning
rm -f $9.tmpChrL.txt $UTR_POS $9.tmpMAP.txt $9.tmpTS.txt $9.tmpOther.txt $9.tmpOther.sorted.txt $OUTPUT_FILE.counts.filteredSV.annotate.txt  $OUTPUT_FILE.TumS.filteredSV.annotate.txt $OUTPUT_FILE.other.filteredSV.annotate.txt
#generate UTR position file
echo "Generate UTR position file ..."
#step 1 -get chromosome length
awk ' {
	if ($1 == "X") { 
		$1=23
	} 
	if ($1 == "Y") { 
		$1=24
	}
	print $1 "\t" $2
} ' $CHR_LEN | grep -v "GL" | grep -v "MT" > $9.tmpChrL.txt

#step 2 -generate UTRs
flankBed -b 1000 -i $GEN_POS -g $9.tmpChrL.txt | grep -v "GL"  > $UTR_POS

echo "done"
##ok
echo "Format exclusion map ..."
awk ' BEGIN {
	comp=0
} 
NR > 1 {
	if ($1 == "X") {
		$1=23
	} 
	else if ($1 =="Y") {
		$1=24
	} 
	if ($1 != "MT") {
		chCK=index($1,"GL")
		if (chCK == 0) {
			print $1 "\t" $2 "\t" $3 "\t" $4
		}
	} 
} '  $EXCL_MAP | sort -k1,1n -k2,2n > $9.tmpMAP.txt




echo "done"



for i in "DEL" "DUP"  
do
#cleaning
rm -f $9.tmp.$i.OvProx.txt $9.tmp.$i.OvProx.Col.txt $9.tmp.$i.OvProx.Col.Mapp.txt $9.tmp.$i.Exc.txt $9.tmp.$i.Exc.DGV.txt $9.tmp.$i.Exc.DGV.Gene.txt $9.tmp.$i.Exc.DGV.Gene.UTR.txt $9.tmp.$i.Exc.DGV.Gene.UTR.RM.txt $9.tmp.utrOV.txt $9.tmp.RMP.txt $9.tmpfres.$i.txt

##create result file by type of events
echo "Generate result file for $i ..."
grep $i $INPUT_FILE | awk ' BEGIN {
	comp=0
} 
{
	if ($3 == "X") {
		$3=23
	} 
	else if ($3 =="Y") {
		$3=24
	} 
	if ($6 == "X") {
		$6=23
	} 
	else if ($6 =="Y") {
		$6=24
	}
	comp=comp+1 
	if ($3 == $6) {
                if ($4 <= $5) {
                    print $3 "\t" $4 "\t" $5 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
                }
                else {
                    print $3 "\t" $5 "\t" $4 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
                }
	} 
	else {
		print $3 "\t" $4 "\t" $4+1 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10 "\n"  $6 "\t" $5 "\t" $5+1 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
	} 
} ' |  grep -v "MT" | grep -v "GL" | sort -k 1,1 -k2,2 -n > $9.tmp.$i.txt

echo "done"

te=$(wc -l  $9.tmp.$i.txt  | cut -d\  -f1)

if [ $te != "0" ]
then 
	## remove calls which overlapp (50%) a region in the mappability exclusion map 
	echo "Filter-out on call due to exclusion for $i events ..."
	intersectBed -v -f 0.5 -a $9.tmp.$i.txt -b $9.tmpMAP.txt  > $9.tmp.$i.Exc.txt
	echo "done"
	## annotate DGV
	echo "DGV annotation for $i events ..."
	intersectBed -c -f 0.5 -a $9.tmp.$i.Exc.txt -b $DGV_MAP  | awk ' { print $1 "\t" $2 "\t" $3 "\t" $4 "," $5} ' > $9.tmp.$i.Exc.DGV.txt
	
	echo "done"
	##annotate gene (cover/disrupte)
	echo "Gene annotation - step 1 - for $i events ..."
	
	intersectBed -wao -a $9.tmp.$i.Exc.DGV.txt -b $GEN_POS > $9.tmp.gene.txt
	
	awk ' BEGIN {
		FS="\t"
	 } NR==1 {
		out=$1 "\t" $2 "\t" $3 "\t" $4 
		split($4,Info,",")
		old=Info[1]
		if ($5 != ".") {
			gcov=$9/($7-$6)
			if (gcov >= 1.0) {
				cov=$8
				disr=""
			} 
			else {
				cov=""
				disr=$8
			}
		}
		else {
			cov="."
			disr="."
		}
	}
	NR > 1 {
		split($4,Info,",")
		if (Info[1] != old) {
			if (cov == "") {
				cov="."
			}
			if (disr == "") {
				disr="."
			}
			print out "," cov "," disr
			out=$1 "\t" $2 "\t" $3 "\t" $4 
			old=Info[1]
			if ($5 != ".") {
				gcov=$9/($7-$6)
				if (gcov >= 1.0) {
					cov=$8
					disr=""
				} 
				else {
					cov=""
					disr=$8
				}
			}
			else {
				cov="."
				disr="."
		
			}
		} 
		else {
			gcov=$9/($7-$6)
			if (gcov >= 1.0) {
				if (cov == "") {
					cov=$8
				}
				else {
					cov=cov ":" $8
				}
			} 
			else {
				if (disr == "") {
					disr=$8
				}
				else {
					disr=disr ":" $8
				}
			}
		}
	} END{
		print out "," cov "," disr 
	} ' $9.tmp.gene.txt > $9.tmp.$i.Exc.DGV.Gene.txt
	
	echo "done"
	
	##annotate gene UTR
	echo "Gene annotation - step 2 - for $i events ..."
	intersectBed -wao -a $9.tmp.$i.Exc.DGV.Gene.txt -b $UTR_POS > $9.tmp.utrOV.txt
	
	awk ' NR==1 {
		out=$1 "\t" $2 "\t" $3 "\t" $4 
		split($4,Info,",")
		old=Info[1]
		if ($5 != ".") {
			alrF=index($4,$8)
			if (alrF == 0 ) {
				utr=$8
			}
			else {
				utr="."
			}
		}
		else {
			utr="."
		}
	}
	NR > 1 {
		split($4,Info,",")
		if (Info[1] != old) {
			print out "," utr 
			out=$1 "\t" $2 "\t" $3 "\t" $4 
			old=Info[1]
			if ($5 != ".") {
				alrF=index($4,$8)
				if (alrF == 0 ) {
					utr=$8
				}
				else {
					utr="."
				}
			}
			else {
				utr="."
			}
		} 
		else {
			alrF=index($4,$8)
			if (alrF == 0 ) {
				if (utr == ".") {
					utr=$8
				}
				else {
					utr=utr ":"$8
				}
			}
		}
	} 
	END {
		print out "," utr
	} ' $9.tmp.utrOV.txt > $9.tmp.$i.Exc.DGV.Gene.UTR.txt
	
	echo "done"
	
	
	##annotate repeat_masker
	echo "RepeatMasker annotation for $i events ..."
	intersectBed -wao -a $9.tmp.$i.Exc.DGV.Gene.UTR.txt -b $REPEAT_MASK_POS > $9.tmp.RMP.txt
	
	awk ' NR==1 {
		out=$1 "\t" $2 "\t" $3 "\t" $4 
		split($4,Info,",")
		old=Info[1]
		if ($5 != ".") {
			rpm=$8
		}
		else {
			rpm="."
		}
	}
	NR > 1 {
		split($4,Info,",")
		if (Info[1] != old) {
			print out "," rpm 
			out=$1 "\t" $2 "\t" $3 "\t" $4 
			old=Info[1]
			if ($5 != ".") {
				rpm=$8
			}
			else {
				rpm="."
			}
		} 
		else {
			alrF=index(rpm,$8)
			if (alrF == 0 ) {
				if (rpm == ".") {
					rpm=$8
				}
				else {
					rpm=rpm ":"$8
				}
			}
		}
	} END {
		print out "," rpm 
	} ' $9.tmp.RMP.txt > $9.tmp.$i.Exc.DGV.Gene.UTR.RM.txt
	
	echo "done"
	
	
	## reformat
	echo "Reformat  $i events for output ..."
	awk ' {
		split($4,Info,",")
		print Info[1] "\t" $0
	} ' $9.tmp.$i.Exc.DGV.Gene.UTR.RM.txt |  sort -k 1,1 -n > $9.tmpfres.$i.txt
	awk ' 
	NR==1 {
		ch=$2
		chn=$2
		st=$3
		en=$4
		split($5,Info,",")
		num=$1
		me=Info[2]
		sa=Info[3]
		si=Info[4]
		ty=Info[5]
		rs=Info[6] 
		os=Info[7]
		dgv=Info[8]
		gec=Info[9]
		ged=Info[10]
		geu=Info[11]
		rpm=Info[12]
	} 
	NR > 1 {
		if ($1 != num) { 
			print me "\t" sa "\t" ch "\t" st "\t" en "\t" chn "\t" si "\t" ty "\t" rs "\t" os "\t" dgv "\t" gec "\t" ged "\t" geu "\t" rpm
			ch=$2
			chn=$2
			st=$3
			en=$4
			split($5,Info,",")
			num=$1
			me=Info[2]
			sa=Info[3]
			si=Info[4]
			ty=Info[5]
			rs=Info[6] 
			os=Info[7]
			dgv=Info[8]
			gec=Info[9]
			ged=Info[10]
			geu=Info[11]
			rpm=Info[12]
		} 
		else { 
			chn=$1
			en=$2
			if (Info[7] != os) {
				if (Info[7] == "A") {
					os="A"
				}
				else if (os == "A") {
					os="A"
				}
				else {
					os="NT"
				}
			}
			if (dgv != ".") {
				dgv=dgv ":" Info[8]
			}
			else {
				dgv=Info[8]
			}
			if (gec != ".") {
				gec=gec ":" Info[9]
			}
			else {
				gec=Info[9]
			}
			if (ged != ".") {
				ged=ged ":" Info[10]
			}
			else {
				ged=Info[10]
			}
			if (geu != ".") {
				geu=geu ":" Info[11]
			}
			else {
				geu=Info[11]
			}
			if (rpm != ".") {
				rpm=rpm ":" Info[12]
			}
			else {
				rpm=Info[12]
			}
		} 
	} END {
		print me "\t" sa "\t" ch "\t" st "\t" en "\t" chn "\t" si "\t" ty "\t" rs "\t" os "\t" dgv "\t" gec "\t" ged "\t" geu "\t" rpm
	} ' $9.tmpfres.$i.txt > $9.fres.$i.txt
	
	
	echo "done"
	
	#merging result
	awk ' {
			if ($10 == "T") {
				print $0
			}
		} ' $9.fres.$i.txt >> $9.tmpTS.txt
	
	awk ' {
		if ($10 != "T") {
			print $0
		}
	} ' $9.fres.$i.txt >> $9.tmpOther.txt
    
    
    ###do the cleaning of tmp files in the loop
    rm -f $9.tmp.$i.OvProx.txt $9.tmp.$i.OvProx.Col.txt $9.tmp.$i.OvProx.Col.Mapp.txt $9.tmp.$i.Exc.txt $9.tmp.$i.Exc.DGV.txt $9.tmp.$i.Exc.DGV.Gene.txt $9.tmp.$i.Exc.DGV.Gene.UTR.txt $9.tmp.$i.Exc.DGV.Gene.UTR.RM.txt $9.tmp.utrOV.txt $9.tmp.RMP.txt $9.tmpfres.$i.txt

else
    echo "No $i found in $INPUT_FILE "
    rm -f $9.tmp.$i.txt
fi

done


#GetStats
echo "Total\tDEL\tINS\tINV\tDUP\tTRA" > $OUTPUT_FILE.counts.filteredSV.annotate.txt
#"DEL" "INS" "INV" "DUP" "TRA" 
#GetStats
if [ -f $9.tmpTS.txt ] ;
then
	awk ' BEGIN {
		v=0
		w=0
		x=0
		y=0
		z=0
	}
	{
		if ($8 == "DEL") {
			v=v+1
		} else if ($8 == "INS") {
			w=w+1
		} else if ($8 == "INV") {
			x=x+1
		} else if ($8 == "DUP") {
			y=y+1
		} else if ($8 == "TRA") {
			z=z+1
		}
	} 
	END {
		all=v+w+x+y+z
		print all "\t" v "\t" w "\t" x "\t" y "\t" z
	} ' $9.tmpTS.txt >>   $OUTPUT_FILE.counts.filteredSV.annotate.txt

	
	#reformat chr name
	sort -k3,3 -k4,4 -n $9.tmpTS.txt > $9.tmpTS.sorted.txt 
	#generate output files local variable
	awk ' BEGIN { 
		print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" 
	} 
	{
		if ($3 == 23) {
			$3="X"
			ml=1
		}
		else if ( $3 == 24) {
			$3="Y"
			ml=1
		}
		if ($6 == 23) {
			$6="X"
			ml=1
		}
		else if ( $6 == 24) {
			$6="Y"
			ml=1
		}
		if (ml == 1) {
			print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $8 "\t" $9 "\t" $10 "\t" $11 "\t" $12 "\t" $13 "\t" $14 "\t" $15
		}
		else {
			print $0
		}
	} ' $9.tmpTS.sorted.txt > $OUTPUT_FILE.TumS.filteredSV.annotate.txt 

else
	echo "NO tumor-specific events reported"
	echo -e "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" > $OUTPUT_FILE.TumS.filteredSV.annotate.txt
fi


if [ -f $9.tmpOther.txt ] ;
then
	awk ' BEGIN {
		v=0
		w=0
		x=0
		y=0
		z=0
	}
	{
		if ($10 == "N") {
			if ($8 == "DEL") {
				v=v+1
			} else if ($8 == "INS") {
				w=w+1
			} else if ($8 == "INV") {
				x=x+1
			} else if ($8 == "DUP") {
				y=y+1
			} else if ($8 == "TRA") {
				z=z+1
			}
		}
	} 
	END {
		all=v+w+x+y+z
		print all "\t" v "\t" w "\t" x "\t" y "\t" z
	} ' $9.tmpOther.txt  >>   $OUTPUT_FILE.counts.filteredSV.annotate.txt

	awk ' BEGIN {
		v=0
		w=0
		x=0
		y=0
		z=0
	}
	{
		if ($10 == "NT") {
			if ($8 == "DEL") {
				v=v+1
			} else if ($8 == "INS") {
				w=w+1
			} else if ($8 == "INV") {
				x=x+1
			} else if ($8 == "DUP") {
				y=y+1
			} else if ($8 == "TRA") {
				z=z+1
			}
		}
	} 
	END {
		all=v+w+x+y+z
		print all "\t" v "\t" w "\t" x "\t" y "\t" z
	} ' $9.tmpOther.txt >>   $OUTPUT_FILE.counts.filteredSV.annotate.txt

	awk ' BEGIN {
		v=0
		w=0
		x=0
		y=0
		z=0
	}
	{
		if ($10 == "A") {
			if ($8 == "DEL") {
				v=v+1
			} else if ($8 == "INS") {
				w=w+1
			} else if ($8 == "INV") {
				x=x+1
			} else if ($8 == "DUP") {
				y=y+1
			} else if ($8 == "TRA") {
				z=z+1
			}
		}
	} 
	END {
		all=v+w+x+y+z
		print all "\t" v "\t" w "\t" x "\t" y "\t" z
	} ' $9.tmpOther.txt >>   $OUTPUT_FILE.counts.filteredSV.annotate.txt


	# BEGIN { 
	# 	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" 
	# } 
	#cordinate sort output

	sort -k3,3 -k4,4 -n $9.tmpOther.txt > $9.tmpOther.sorted.txt 


	awk ' BEGIN { 
		print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" 
	} 
	{
		if ($3 == 23) {
			$3="X"
			ml=1
		}
		else if ( $3 == 24) {
			$3="Y"
			ml=1
		}
		if ($6 == 23) {
			$6="X"
			ml=1
		}
		else if ( $6 == 24) {
			$6="Y"
			ml=1
		}
		if (ml == 1) {
			print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $8 "\t" $9 "\t" $10 "\t" $11 "\t" $12 "\t" $13 "\t" $14 "\t" $15
		}
		else {
			print $0
		}
	} ' $9.tmpOther.sorted.txt > $OUTPUT_FILE.other.filteredSV.annotate.txt
else
	echo "NO Germline events reported"
	echo -e "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" > $OUTPUT_FILE.other.filteredSV.annotate.txt
fi



###do the cleaning of tmp files outside the loop
rm $9.* 

